cores_estat <- c('#A11D21','#663333','#FF6600','#CC9900',
                 '#CC9966','#999966','#006606','#008091',
                 '#003366','#041835','#666666') # Cores padrão Estat

theme_estat <- function(...) {
  theme <- ggplot2::theme_bw() +
    ggplot2::theme(
      axis.title.y = ggplot2::element_text(colour = "black", size = 12),
      axis.title.x = ggplot2::element_text(colour = "black", size = 12),
      axis.text = ggplot2::element_text(colour = "black", size = 9.5),
      panel.border = ggplot2::element_blank(),
      axis.line = ggplot2::element_line(colour = "black"),
      legend.position = "top",
      ...
    )
  
  return(
    list(
      theme,
      scale_fill_manual(values = cores_estat),
      scale_colour_manual(values = cores_estat)
    )
  )
}        # Funçaõ de tema 

# Diretório 

setwd('D:\\Estudo\\Estat\\Projeto Fantasma')
options(scipen=999)                       # Tirar todas as Notações cientificas 
rm(list = ls())                           # Limpar ambiente

# Pacotes 

require(dplyr)
require(readxl)
require(lubridate)
require(stringr)
require(tidyr)
require(ggplot2)
require(scales)
require(forecast)


# carregando dados 

continentes <- read_xlsx('Continentes.xlsx') 
world_cups <- read_xlsx('World_Cups.xlsx')

#manipulando dados 

principal <- world_cups %>%              #Dados Base, separadando a coluna Datetime 
  separate(Datetime, c('Day','Month', 'Year', 'c4', 'hour'), " ") %>%  
  select(-c4) 

filtrado <- principal %>%                 #Selecionando apenas o ano e o público 
  select(Year, Attendance) %>% 
  na.omit() %>% 
  group_by(Year) %>% 
  summarise(Attendance = sum(Attendance))


# plotando gráfico para análise de Publíco x Ano


publico_ano <- ggplot(data = filtrado,aes(x = Year, y= Attendance, group = 1)) +
  geom_line(colour ='#A11D21', size = 1) +
  labs(x = 'Ano', y = 'Público')+
  scale_y_continuous(label=comma) +
  theme_estat(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  ggsave("publico1-ano.pdf", width = 158, height = 93, units = "mm")

publico_ano                             # Plot do gráfico  
 


# parametros de correlação 

p <- as.data.frame(c(2018,2022))         # Selecionando anos de interesse para projeção
colnames(p) <- "Year"                    # Definindo nome da coluna 
filtrado$Year <- as.numeric(filtrado$Year)   # Tranformando para numérico para análise de correlação


# Gráfico de correlação

ggplot(filtrado, aes(x = Year, y = Attendance)) +
  geom_point(colour = '#A11D21')+
  scale_y_continuous(label=comma)+
  geom_smooth(method = 'lm') +
  theme_estat()                          #Gráfico para ver se existe correlação entre variáveis


cor(filtrado$Year,filtrado$Attendance)       #Correlação entre as variáveis -FORTE CORRELAÇÃO-

model <- lm(Attendance ~ Year, data = filtrado) #Modelo linear para projeção

summary(model)                                #Parâmetros do modelo -MODELO ADEQUADO-

pred <- predict(model, newdata = p)          #Predições para 2018 e 2022




Year <- c(2018,2022)                       #Selecionando anos para o gráfico
Attendance <- as.matrix(pred)              #Predições como Matriz

pred2 <- as.data.frame(cbind(Year,Attendance)) #Juntando as matrizes em dataframe
colnames(pred2)[2]  <- "Attendance"            #Renomeando Coluna


append <- rbind(filtrado,pred2)                 #Juntando os valores da projeção no dataframe filtrado
append$Year <- as.character(append$Year)      #Mudando a classe do ano para character para melhor visualização


# Gráfico das projeções 

proj_plot <- ggplot(data = append,
                    aes(x = Year,
                        y = Attendance,
                        fill = factor(ifelse((Year == '2018' | Year == '2022'), 'Previsto','Observado')))) +
  geom_bar(stat = 'identity') +
  guides(fill="none") +
  labs(x = 'Ano', y = 'Público') +
  scale_y_continuous(label=comma) +
  scale_fill_manual(name = 'Observado x Previsto', values = c('#A11D21','#FF6600'))+ 
  theme_estat(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1)) +
  ggsave("projeção.pdf", width = 158, height = 93, units = "mm")

proj_plot
